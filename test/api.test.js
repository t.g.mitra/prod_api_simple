const { 
    handleProducts
} = require('../api.model');

// Define test name of product which wiull be removed at last
const test_product = {
    name: 'test_nd7298sms3ma94nvx3m',
    price: 10,
    description: "demo description"
}

let product_id = 0
let status

// 1.0 Verify Add products
test('Add product test', async () => {

    // Success scenario: save product
    status = await handleProducts(test_product, 'add');

    if(!status || !status.data || !status.data.isCreated) {
        throw new Error('Add product failed.')
    }
    else 
        product_id = status.data.record.id    

    // Failure scenario: Add the same product
    status = await handleProducts(test_product, 'add');
    if(status && status.data && status.data.isCreated) {
        throw new Error('Same product wrongly added twice.')
    }
})

// Verify view product
test('View product test', async () => {
    status = await handleProducts(product_id, 'get')
    if(!status || !status.data || !status.data.id) {
        throw new Error('Unable to retrieve the prodict details.')
    }
})

// Verify delete product
test('Delete product test', async () => {
    // Soft delete
    status = await handleProducts(product_id, 'delete', '', false)
    if(!status || !status.status_code || status.status_code !== 200) {
        throw new Error('Unable to soft delete the selected test prodict.')
    }

    // Hard delete
    status = await handleProducts(product_id, 'delete', '', true)
    if(!status || !status.status_code || status.status_code !== 200) {
        throw new Error('Unable to hard delete the selected test prodict.')
    }
})