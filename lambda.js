const { runLambda } = require("./api.route");

exports.handler = async (event) => {
    return await runLambda(event.httpMethod, event.path, event.queryStringParameters, event.body, event.headers)
};