const { addProduct, getProduct, deleteProduct, getMostViewedProds, updateCurrencyValue } = require('./api.controller');

async function runLambda(method, pathname, params, body, headers) {
  const ids = pathname.match(/\d+/g);
  const id = ids && ids[0] || ""

  if(method === 'POST' && pathname === "/api/product") {
    return await addProduct(body)
  }

  if(method === 'GET' && pathname.indexOf("/api/product") !== -1) {
    return await getProduct(id, params)
  }

  if(method === 'DELETE' && pathname.indexOf("/api/product") !== -1) {
    return await deleteProduct(id)
  }

  if(method === 'GET' && pathname === "/api/mostviewed") {
    return await getMostViewedProds()
  }

  if(method === 'GET' && pathname === "/api/currency") {
    return await updateCurrencyValue()
  }

  return {
    statusCode: 404,
    body: {
        status: 'Error', message:"Invalid request."
      }
  };
  
}

module.exports = { runLambda }