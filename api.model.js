const http = require('http');
require('dotenv').config()
const { connectDB, currencies, products } = require("./data.model")
connectDB()

// Get currency information from external source
const getCurrency = async (source, currencies) => {
  const hostname = process.env.CURR_HOST || ''
  const path = process.env.CURR_PATH || ''
  const apikey = process.env.CURR_APIKEY || ''

  const options = {
    hostname,
    path: path + '?source='+source+'&currencies='+currencies,
    method: 'GET',
    headers: {
      "apikey": apikey
    }
  };

  const curr_data = await new Promise(function(resolve, reject) {
    http.request(options, (res) => {
      let data = ''
      res.on('data', (chunk) => {
          data += chunk;
      });
      // Ending the response 
      res.on('end', () => {
          return resolve(JSON.parse(data))
      });
    }).on("error", (err) => {
      return reject(err)
    }).end()
  });

  // Sending the request
  return curr_data && curr_data.quotes ? curr_data.quotes : false;
}

/**
 * Add product to DB
 * @param {*} params 
 * @returns 
 */
const addProducts = async (params) => {
  let data, message = ''
  params.price = Math.round(params.price * 100) / 100
  params.name = params.name.trim()

  data = await products.findOrCreate({
    where:{ "product_name": params.name },
    defaults:{ 
      product_name: params.name,
      product_price: params.price,
      produce_desc: params.description
    }
  }).then(function(result) {
    return {
      isCreated: result[1],
      record : result[0].toJSON()
    }
  }).catch(function (err) {
    return false
  });
  message = !data ? "Unable to add product into database." : (data.isCreated ? "Product added to database." : "Similar product already available in database.")
  return {message, data}
}

/**
 * Get product from database
 * @param {*} id 
 * @returns 
 */
const getProducts = async(id) => {
  products.increment({"view_count": 1}, { where: { id } })
  const result = await products.findOne({
    where: { id, is_deleted:0 }
  });
  return result ? result.toJSON() : false
}

/**
 * Delete product - both hard and soft delete
 * @param {id} id 
 * @returns 
 */
const deleteProducts = async(id, hard_delete) => {
  return !hard_delete ? await products.update(
                          { is_deleted: 1 }, 
                          {where: { id }}
                        ) : await products.destroy({where: { id }})
}

/**
 * Get most viewed products
 * @returns 
 */
const getMostViewedProducts = async() => {
  return products.findAll({
    order: [
      ['view_count', 'DESC']
    ],
    where: {
      is_deleted: 0
    },
    offset: 0,
    limit: 5
  })
}

/**
 * Handle all product request such as add, delete, view etc.
 * @param {*} params 
 * @param {*} type 
 * @param {*} currency 
 * @returns 
 */
const handleProducts = async (params, type, currency='USD', hard_delete=false) => {
  let status_code = 200
  let message
  let data

  // Add product to database
  if(type === 'add') {
    const status = await addProducts(params);
    message = status.message
    data = status.data
    status_code = !status.data ? 404 : status_code
  }

  // Get product from database
  if(type === 'get') {
     const stats = await getProducts(params);
     if(!stats) {
      message = "Invalid product ID."
      status_code = 404
     }
     else {
      currency = currency === '' ? 'USD': currency
      if(currency !== 'USD') {
        let curr_row = await currencies.findOne({ where: { 'currency_name' : currency } });
        if(curr_row)
          stats.product_price =  Math.round((stats.product_price*(curr_row.toJSON()).currency_value) * 100) / 100
      }

      data = {...stats, currency}
      message = undefined
     }   
  }

  // Delete product from database
  if(type === 'delete') {
    const status = await deleteProducts(params, hard_delete);
    status_code = !hard_delete ? (status && status[0] ? 200 : 404) : (status && status===1 ? 200 : 404)
    message = status && (status[0] && status[0]===1 || status===1) ? "Your selected product has been removed." : "Invalid product ID."
  }

  if(type === "mostviewed") {
    const status = await getMostViewedProducts();
    status_code = !status ? 404 : 200
    message = status.length + " most viewed product"+(status.length > 1 ? 's':'')+"."
    data = status
  }

  return {
    data, 
    status_code,
    message
  };
}

/**
 * Update currency value from external API
 * @returns 
 */
const updateCurrency = async() => {
  let status_code = 200
  const curr_list = process.env.CURRENCIES || ''
  const src_currency = process.env.SRC_CURR || ''

  if(!curr_list || !src_currency)
    return {
      data: currency_value, 
      status_code,
      message: 'Unable to get currency configuration.'
    };

  // Get currency from API
  const currency_value = await getCurrency(src_currency, curr_list)

  for (const key in currency_value) {
    const curr_key = key.replace(src_currency,'')
    const curr_val = currency_value[key]
    const status = await currencies.findOne({ where: { 'currency_name' : curr_key }})
    
    if(status)
      currencies.update({ currency_value: curr_val }, {where: { currency_name: curr_key }});
    else
      currencies.create({ currency_name: curr_key, currency_value: curr_val});
  }

  return {
    data: currency_value, 
    status_code,
    message: 'Currency value updated'
  };
}

module.exports = { 
    handleProducts, updateCurrency
}