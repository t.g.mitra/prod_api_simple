const http = require('http')
const url = require('url');
const { runLambda } = require("./api.route");

const server = http.createServer()
server.on('request', async (req, res) => {
    const parsedUrl = url.parse(req.url,true);   
    let body = false;

    if(req.headers && req.headers['content-length'] && req.headers['content-type']) {
        if(req.headers['content-type']==='application/json') {
            body = await new Promise(function(resolve, reject) {
                req.on('data', async data => {
                    if(data) return resolve(JSON.parse(data.toString()))
                    else return reject("")
                });
            });
        }
    }

    const result = await runLambda(req.method, parsedUrl.pathname, parsedUrl.query, body, req.headers)
    res.writeHead(result.statusCode, {'Content-Type': 'application/json'});
    res.write(JSON.stringify(result.body));
    res.end();
});

const port = process.env.PORT || 8888;
server.listen(port, () => console.log(`Listning on port ${port}.....`));