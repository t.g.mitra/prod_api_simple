const Joi = require('joi');

/**
 * Validate product
 * @param {*} params 
 * @returns 
 */
const validateProduct = (params) => {
  const JoiSchema = Joi.object({
    name: Joi.string()
          .min(5)
          .max(50)
          .required(),
    price: Joi.number()
          .precision(0)
          .required(),
    description: Joi.string()
          .optional()
          .allow(null, '')
  }).with('name', 'price');
  const response = JoiSchema.validate(params)
  return response.error ?  
          { error: response.error, code: 400,  message: response.error.details[0].message.replaceAll("\"", "*")} 
          : 
          { error:false, code: 200, message: 'OK' }
}

const validateProductandCurrencyID = (params) => {
  const JoiSchema = Joi.object({
    id: Joi.number()
          .precision(0)
          .required(),
    currency: Joi.string()
          .required()
          .valid('', 'USD', 'EUR', 'CAD', 'GBP')
  }).options({ abortEarly: false });
  const response = JoiSchema.validate(params)
  return response.error ?  
          { error: response.error, code: 400,  message: response.error.details[0].message.replaceAll("\"", "*") } 
          : 
          { error:false, code: 200, message: 'OK' }
}

const validateProductID = (params) => {
  const JoiSchema = Joi.object({
    id: Joi.number()
          .precision(0)
          .required()
  }).options({ abortEarly: false });
  const response = JoiSchema.validate(params)
  return response.error ?  
          { error: response.error, code: 400,  message: response.error.details[0].message } 
          : 
          { error:false, code: 200, message: 'OK' }
}

module.exports = { 
    validateProduct, validateProductID, validateProductandCurrencyID
}