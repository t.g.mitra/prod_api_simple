const { Sequelize, DataTypes } = require('sequelize');
require('dotenv').config()

// Get config information from enviornment variables
const host = process.env.DB_HOST || ''
const port = process.env.DB_PORT || ''
const username = process.env.DB_USER || ''
const password = process.env.DB_PASSWORD || ''
const database = process.env.DB_NAME || ''
const dialect = process.env.DIALECT || 'mysql'

// Create instance of DB connection using sequelize
const sequelize = new Sequelize(database, username, password, { host, port, dialect, logging: false});

// Currency data model
const currencies = sequelize.define('currencies', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    currency_name: DataTypes.STRING,
    currency_value: DataTypes.STRING
});

// Product data model
const products = sequelize.define('products', {
  id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
  },
  product_name: DataTypes.STRING,
  product_price: DataTypes.FLOAT,
  produce_desc: DataTypes.TEXT,
  view_count: {
    type: DataTypes.MEDIUMINT,
    defaultValue: 0
  },
  is_deleted: {
    type: DataTypes.TINYINT,
    defaultValue: 0
  }
});

// DB connection
const connectDB = async () => {
  try { await sequelize.authenticate() } 
  catch (error) { console.error('Unable to connect to the database:', error) }
  await sequelize.sync();
}

module.exports = { 
    connectDB, sequelize, currencies, products
}