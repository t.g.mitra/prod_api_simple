const { handleProducts, updateCurrency } = require('./api.model');
const { validateProduct, validateProductID, validateProductandCurrencyID } = require('./validation.model');

// Add product to DB
const addProduct = async (body) => {

    // Validate record
    let status = validateProduct(body)
    if(status.error) 
      return { 
        statusCode: 404, 
        body: { status:'Error', message: status.message }
      }
    
    // Add record
    let {data, status_code, message} = await handleProducts(body, 'add');
    return {
      statusCode: status_code,
      body: { status : status_code === 200 ? 'OK' : 'Error', data, message }
    }
}

// Get product from DB
const getProduct = async (id, params) => {
  
  // Validate the input parameters
  let datastatus = validateProductandCurrencyID({
      id,
      currency: params.currency || ''
  })

  // If input data contains validation error then output error message
  if(datastatus.error) {
    return {
      statusCode: datastatus.code,
      body: {status:'Error', message: datastatus.message}
    }
  }

  // Get products data
  const {data, status_code, message} = await handleProducts(id, 'get', (params.currency || ''))
  return {
    statusCode: status_code,
    body: {
      status : status_code === 200 ? 'OK' : 'Error', data, message
    }
  };
}

// Delete product from DB
const deleteProduct = async (id) => {

  // Validate the input parameters
  let datastatus = validateProductID({ id })

  // If input data contains validation error then output error message
  if(datastatus.error) {
    return {
      statusCode: datastatus.code,
      body: {status:'Error', message: datastatus.message}
    }
  }
  
  // Get products data
  const {data, status_code, message} = await handleProducts(id, 'delete')
  return {
    statusCode: status_code,
    body: {
      status : status_code === 200 ? 'OK' : 'Error', data, message
    }
  };
}

// Get most viewed products
const getMostViewedProds = async () => {
  const {data, status_code, message} = await handleProducts(0, 'mostviewed')
  return {
    statusCode: status_code,
    body: {
      status : status_code === 200 ? 'OK' : 'Error', data, message
    }
  };
}

// Update currency value
const updateCurrencyValue = async () => {
  const {data, status_code, message} = await updateCurrency()

  return {
    statusCode: status_code,
    body: {
      status : status_code === 200 ? 'OK' : 'Error', data, message
    }
  };
}

module.exports = { addProduct, getProduct, deleteProduct, getMostViewedProds, updateCurrencyValue }